<?php
/**
 * Index
 *
 * Standard loop for the front-page
 */
get_header();?>

<div class="row">
    <div class="large-8 medium-8 small-12 columns">
        <?php if(have_posts()):
            while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <h3>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(array('before' => __('Permalink to ', 'foundation'))); ?>" rel="bookmark">
                            <?php the_title(); ?>
                        </a>
                    </h3>
                    <?php if (has_post_thumbnail()): ?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
                            <?php the_post_thumbnail(); ?>
                        </a>
                    <?php endif;

                    the_excerpt(); ?>
                </article>
            <?php endwhile;
        endif;

        foundation_pagination(); ?>
    </div>
    <div class="large-4 medium-4 small-12 columns sidebar">
	    <?php get_sidebar(); ?>
    </div>
</div>

<?php get_footer(); ?>