<?php
/**
 * Footer
 */
?>

<footer>
    <?php $footer_options = get_field('footer_options', 'option')[0]; ?>
    <div class="row">
        <div class="large-12 medium-12 small-12 columns">
            <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'inline-list','fallback_cb' => 'foundation_page_menu')); ?>
        </div>
    </div>

    <?php if($footer_options['copyright_options']): ?>
        <div class="row">
            <div class="large-12 columns text-left">
                <?php echo $footer_options['copyright_options']; ?>
            </div>
        </div>
    <?php endif; ?>

</footer>

<?php wp_footer(); ?>
</body>
</html>